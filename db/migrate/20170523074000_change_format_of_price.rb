class ChangeFormatOfPrice < ActiveRecord::Migration[5.0]
  def change
    change_column(:items, :price, :decimal, :precision => 16, :scale => 2)
  end
end
