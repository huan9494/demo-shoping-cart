class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :customer_name
      t.string :email
      t.text :contact_address
      t.string :image_url_array
      t.string :name_array
      t.string :price_array
      t.string :quantity_array
      t.decimal :total_price

      t.timestamps
    end
  end
end
