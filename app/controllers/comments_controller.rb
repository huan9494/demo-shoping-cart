class CommentsController < ApplicationController
  def index
    @item = Item.find(params[:id])
    @comments = @item.comments.all
  end

  def show
    @item = Item.find(params[:id])
    @comment = @item.comments.find(params[:id])
  end

  def new
    @item = Item.find(params[:id])
    @comment = @item.comments.new
  end
  def create
    @comment = Comment.create(comments_params)
  end

  private
  def comments_params
    params.require(:comment).permit(:user_name, :content, :item_id)
  end
end
