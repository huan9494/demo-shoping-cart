class Admin::CategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin
  
  layout 'admin'
  
  def index
    @categories = Category.all
    @items = Item.includes(:category).all.order(:name)
    @orders = Order.all
  end

  def show
    @categories = Category.all
    @category = Category.find(params[:id])
    @item = @category.items.all.order(:name)
    @orders = Order.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:notice] = "Category has been created successful"
      redirect_to admin_categories_path
    else
      flash.now[:error] = @category.errors.full_messages
      render 'new'
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update(category_params)
      redirect_to admin_categories_path
    else
      render 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    redirect_to admin_categories_path
  end

  def search
    @categories = Category.all
    @items = Item.search(params[:query], limit: 1)
    @orders = Order.all
    render :index
  end

  def autocomplete
    item_hash = Item.search(params[:query], match: :text_start,
                     limit: 5, load: false, misspellings: {below: 5}).map do |element|
      { name: element.name}
    end
    render json: item_hash
  end
  
  private
  def category_params
    params.require(:category).permit(:name)
  end
  def check_admin
    redirect_to root_path unless current_user.admin?
  end
end
