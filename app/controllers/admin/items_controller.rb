class Admin::ItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin

  layout 'admin'
  
  def index
    @category = Category.find(params[:category_id])
    @items = @category.items.all
  end

  def show
    @category = Category.find(params[:category_id])
    @item = @category.items.find(params[:id])
    @categories = Category.all
  end

  def new
    @category = Category.find(params[:category_id])
    @item = @category.items.new
  end

  def create
    @category = Category.find(params[:category_id])
    @item = @category.items.new(item_params)
    if @item.save
      flash[:notice] = "Item has been created successful"
      redirect_to admin_root_path
    else
      flash.now[:error] = @item.errors.full_messages
      render 'new'
    end
  end

  def edit
    @category = Category.find(params[:category_id])
    @item = @category.items.find(params[:id])
  end

  def update
    @category = Category.find(params[:category_id])
    @item = @category.items.find(params[:id])
    if @item.update(item_params)
      redirect_to admin_root_path
    else
      render 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:category_id])
    @item = @category.items.find(params[:id])
    @item.destroy
    redirect_to admin_root_path
  end

  private
  def item_params
    params.require(:item).permit(:name, :description, :image, :price)
  end
  def check_admin
    redirect_to root_path unless current_user.admin?
  end
end
