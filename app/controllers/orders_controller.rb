class OrdersController < ApplicationController

  layout 'admin'
  
  before_action :init, only: [:new]

  include OrdersHelper
  
  def index
    if current_user.admin?
      @orders = Order.all
    else
      redirect_to root_path
    end
  end

  def show
    if current_user.admin?
      @order = Order.find(params[:id])
    else
      redirect_to root_path
    end
  end

  def new
    @cart_items = Item.where(:id => @item_id_array)
    get_data(@cart_items)
    if @cart_items
      @order = Order.new
    end
  end
  def create
    @order = Order.create(orders_params)
    if @order.save
      session[:cart] = nil
    else
      render 'new'
    end
  end

  def init
    session[:cart] ||= []
    cart_init(session[:cart])
  end

  private
  def orders_params
    params.require(:order).permit(:customer_name, :email, :contact_address, :total_price,
                                  image_url_array: [], name_array: [], price_array: [],
                                  quantity_array: [])
  end

end
