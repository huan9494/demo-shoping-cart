class ItemsController < ApplicationController

  def index
  end

  def show
    @item = Item.find(params[:id])
    @categories = Category.all
    if current_user
      @item.ratings.create!(item_id: params[:id], user_id: current_user.id, score: 0) unless
                                    current_user.ratings.where(item_id: params[:id]).first.present?
      @rating = current_user.ratings.where(item_id: params[:id]).first
    end
  end

  def update
    @cart = current_user.carts.find_by_item_id(params[:item][:item_id])
    if @cart.present?
      @cart.update(quantity: params[:item][:quantity], item_id: params[:item][:item_id])
    else
      @cart = current_user.carts.new
      @cart.item_id = params[:item][:item_id]
      @cart.quantity = params[:item][:quantity]

      @cart.save
    end
  end
end
