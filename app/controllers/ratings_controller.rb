class RatingsController < ApplicationController
  def update
    @rating = Rating.find(params[:id])
    @item = @rating.item
    @rating.update(score: params[:score])
  end
end