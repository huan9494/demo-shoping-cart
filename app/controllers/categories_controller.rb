class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  def index
    @categories = Category.all
    @items = Item.order(:name).paginate(page: params[:page], per_page: 9)
  end

  def show
    @categories = Category.all
  	@items = @category.items.paginate(page: params[:page], per_page: 6)
  end

  def search
    @categories = Category.all
    @items = Item.search(params[:query], limit: 1)
    render :index
  end

  def autocomplete
    item_hash = Item.all.map do |element|
      { name: element.name}
    end
    render json: item_hash
  end
  private

  def set_category
  	@category = Category.find_by_id params[:id]
  	redirect_to root_path unless @category.present?
  end

end
