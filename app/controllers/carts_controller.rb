class CartsController < ApplicationController

  layout 'admin'
  
  before_action :authenticate_user!
  before_action :init, only: [:index, :create, :destroy]
  
  include CartHelper

  def index
    if current_user
      @cart_items = Item.where(:id => @item_id_array)
      show_total(@cart_items, @cart_quantity)
    end
  end
  def create
    session[:cart] << {'item_id' => params[:item_id], 'quantity' => 1} unless
                                                              @item_id_array.include?(params[:item_id])
  end

  def update
    update_cart(session[:cart])
  end


  def destroy
    @item_id_array.each_with_index do |item_id, index|
      if item_id == params[:item_id]
        session[:cart].delete_at(index)
      end
    end 
    redirect_to carts_index_path
  end

  def init
    session[:cart] ||= []
    cart_init(session[:cart])
  end

  private
  def carts_params
    params.require(:cart).permit(:quantity, :item_id)
  end

end
