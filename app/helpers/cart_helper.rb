module CartHelper
  NO = 0
  YES = 1

  def total_price(item, index)
    number_to_currency(item.price * @cart_quantity[index].to_i)
  end
  def show_total(cart_items, cart_quantity)
    @total = 0
    @number_item = 0
    @total_number_item = 0
    cart_items.each_with_index do |item, index|
      @total += item.price * cart_quantity[index].to_i
      @number_item += 1
      @total_number_item += cart_quantity[index].to_i
    end
  end

  def update_cart(session_cart_arr)
    check_contain = NO
    session_cart_arr ||= []
    session_cart_arr = session_cart_arr.sort_by! { |hash| hash['item_id']}
    session_cart_arr.each do |cart|
      if cart['item_id'] == params[:item_id]
        cart['quantity'] = params[:quantity].to_i
        check_contain = YES
      end
    end
    session_cart_arr << {'item_id' => params[:item_id], 'quantity' => params[:quantity].to_i} if
                                                                          check_contain == NO
    cart_init(session_cart_arr)

    @cart_items = Item.where(:id => @item_id_array)

    @item_id = params[:item_id]
    @total_price = params[:price].to_f * params[:quantity].to_i
    show_total(@cart_items, @cart_quantity)
  end

  def cart_init(session_cart_arr)
    @item_id_array = []
    @cart_quantity = []
    session_cart_arr = session_cart_arr.sort_by! { |hash| hash['item_id']}
    session_cart_arr.each do |cart|
      @item_id_array << cart['item_id']
      @cart_quantity << cart['quantity']
    end
  end
end
