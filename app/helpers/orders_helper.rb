module OrdersHelper
  def get_data(cart_item)
    @image_url_array = []
    @name_array = []
    @price_array = []
    @quantity_array = []
    @total_price = 0
    cart_item.each_with_index do |cart_item, index|
      @image_url_array << cart_item.image.small_thumb.url
      @name_array << cart_item.name
      @price_array << cart_item.price.to_f
      @quantity_array << @cart_quantity[index].to_i
      @total_price += cart_item.price.to_f * @cart_quantity[index].to_i
    end
  end

  def url_image(index)
    @order.image_url_array[index]
  end
  def item_name(index)
    @order.name_array[index]
  end
  def item_price(index)
    @order.price_array[index].to_f
  end
  def quantity(index)
    @order.quantity_array[index].to_i
  end
  def total_item_price(index)
    item_price(index) * quantity(index)
  end

  def cart_init(session_cart_arr)
    @item_id_array = []
    @cart_quantity = []
    session_cart_arr = session_cart_arr.sort_by! { |hash| hash['item_id']}
    session_cart_arr.each do |cart|
      @item_id_array << cart['item_id']
      @cart_quantity << cart['quantity']
    end
  end
end
