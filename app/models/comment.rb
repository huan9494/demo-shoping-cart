class Comment < ApplicationRecord
  belongs_to :item
  validates :user_name, presence: true
  validates :content, presence: true
end
