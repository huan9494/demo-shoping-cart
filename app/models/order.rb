class Order < ApplicationRecord
  serialize :image_url_array
  serialize :name_array
  serialize :price_array
  serialize :quantity_array

  validates :customer_name, presence: true
  validates :email, presence: true
  validates :contact_address, presence: true
  validates :image_url_array, presence: true
  validates :name_array, presence: true
  validates :price_array, presence: true
  validates :quantity_array, presence: true
  validates :total_price, presence: true
end
