class Item < ApplicationRecord
  belongs_to :category
  mount_uploader :image, FileUploader

  validates :name, presence: true
  validates :description, presence: true
  validates :image, presence: true
  validates :price, presence: true

  has_many :carts, dependent: :destroy
  belongs_to :user

  has_many :comments, dependent: :destroy
  has_many :ratings
  
  searchkick searchable: [:name], word_start: [:name]

  def average_rating
    if ratings.size > 0
      ratings.sum(:score) / ratings.size
    end
  end

end
