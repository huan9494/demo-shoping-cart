// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require turbolinks
//= require bootstrap-typeahead-rails
//= require ckeditor/init
//= require jquery.raty
//= require_tree .

$(document).on('turbolinks:load', function(){

   var bloodhound = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    prefetch: {
      url: "/categories/autocomplete",
      thumbprint: "home"
    }
  });
  bloodhound.initialize();
  $("#item_search").typeahead({
      hint: true,
      minLength: 1,
      highlight: true
  },
    {
      displayKey: "name",
      source: bloodhound.ttAdapter()
  });


    var admin_bloodhound = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace("name"),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: '/admin/categories/autocomplete?query=%QUERY',
    wildcard: '%QUERY',

  });
  admin_bloodhound.initialize();
  $("#admin_item_search").typeahead({
      hint: true,
      minLength: 1,
      highlight: true
  },
    {
      displayKey: 'name',
      source: admin_bloodhound.ttAdapter()
  });


  $(".add-to-cart").on('click', function(){
    item_id = this.getAttribute("item-id");
    data_user = this.getAttribute("data-user");
    // $("#add_to_cart_" + item_id).fadeOut(1000);
    if (data_user.length) {
      alert ('Add to cart successfully');
      $.ajax({
          url: '/carts/create',
          // dataType: 'json',
          dataType: 'script',
          method: "post",
          data: {item_id},

      });
    } else {
      alert ('You must sign in before adding new items!');
      $(window.location.replace("/users/sign_in"));
    }
      
  });

  $('html, body').animate({
      scrollTop: $('#auto_scroll').offset().top
  }, 'slow');

  $('html, body').animate({
        scrollTop: $('.features_items').offset().top
  }, 'slow');

  $('#btn-comment').on('click', function(){
    $('.new_comment').submit();
    $('#comment_user_name').val("");
    $('#comment_content').val("");
  });


})
