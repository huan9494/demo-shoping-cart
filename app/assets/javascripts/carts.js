$(document).on('turbolinks:load', function(){  
  // var delay = (function(){
  //   var timer = 0;
  //   return function(callback, ms){
  //   clearTimeout (timer);
  //   timer = setTimeout(callback, ms);
  //   };
  // })();
  // $('.cart_quantity_button input').bind('keyup mouseup',function() {
  //   data_id = this.getAttribute("data_id");
  //   data_price = this.getAttribute("data_price");
  //   data_quantity = $("form#edit_cart_" + data_id + " input[name='cart[quantity]']").val();
  //   var total_price = 0;
  //   var total_quantity = 0;
  //   if(/^[0-9]*$/.test(data_quantity) == false) {
  //     alert('Quantity must be in integer. Please check!');
  //     $("form#edit_cart_" + data_id + " input[name='cart[quantity]']").val(0);
  //     $("#total_price_cart_" + data_id).html("$0.00");
  //     $("#total").html("Total<br/>$0.00");
  //     $("#total_quantity").html("Total quantity<br/>0" );
  //   }else{
  //     $("#total_price_cart_" + data_id).html("$" + (data_quantity * data_price).toFixed(2));
  //     $(".cart_quantity_button input[name='cart[quantity]']").each(function(){
  //       data_id = this.getAttribute("data_id");
  //       data_price = this.getAttribute("data_price");
  //       data_quantity = $("form#edit_cart_" + data_id + " input[name='cart[quantity]']").val();
  //       total_price += data_price * data_quantity;
  //       total_quantity += parseInt(data_quantity, 10);
  //     });
  //     $("#total").html("Total<br/>" + "$" + total_price.toFixed(2) );
  //     $("#total_quantity").html("Total quantity<br/>" + total_quantity );
  //   };

  //   delay(function(){

  //   }, 200 );
  // });
  var delay = (function(){
    var timer = 0;
    return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
    };
  })();
  $('.cart_quantity_button input#quantity_input').on("keyup mouseup", function() {
    item_id = this.getAttribute("data_id");
    price = this.getAttribute("data_price");
    quantity = $("form#edit_cart_" + item_id + " input[name='cart_quantity']").val();
    if(/^[0-9]*$/.test(quantity) == false) {
      alert('Quantity must be in integer. Please check!');
      quantity = Math.floor(quantity);
      $("form#edit_cart_" + item_id + " input[name='cart_quantity']").val(Math.floor(quantity));
    }else{
      if (quantity < 1) {
        quantity = 1;
        $("form#edit_cart_" + item_id + " input[name='cart_quantity']").val(1);
        alert("Quantity must be greater than 0")
      }
    };

    delay(function(){
    // $("#edit_cart_" + data_id).submit();
    $.ajax({
          url: '/carts/update',
          // dataType: 'json',
          dataType: 'script',
          method: "patch",
          data: {item_id, quantity, price},

      });

    }, 200 );
  });
  
  $(".cart_delete a").on('click', function(){
    data_id = this.getAttribute("data_id");
    $("#tr_cart_" + data_id).hide();
  });
})