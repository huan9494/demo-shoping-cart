$(document).on('turbolinks:load', function(){

  var path = window.location.href;
  var current_link = path.substring(path.indexOf('/', 10));
  $('a[href="' + current_link + '"]').parents('h4.panel-title').addClass('selected');


  $('.edit_item input#quantity_input').on('keyup mouseup', function() {
  item_id = this.getAttribute("data_id");
  quantity = $("form#edit_item_" + item_id + " input[name='item_quantity']").val();
  if(/^[0-9]*$/.test(quantity) == false) {
    alert('Quantity must be in integer. Please check!');
    quantity = Math.floor(quantity);
    $("form#edit_item_" + item_id + " input[name='item_quantity']").val(Math.floor(quantity));
  }else{
    if (quantity < 1) {
      quantity = 1;
      $("form#edit_item_" + item_id + " input[name='item_quantity']").val(1);
      alert("Quantity must be greater than 0")
    }
    };
  });

  $("button.add-to-cart-details").on('click', function(){
  item_id = this.getAttribute("item-id");
  price = this.getAttribute("data_price");
  quantity = $("form#edit_item_" + item_id + " input[name='item_quantity']").val();
  data_user = this.getAttribute("data_user");
  if (data_user.length) {
    $.ajax({
      url: '/carts/update',
      // dataType: 'json',
      dataType: 'script',
      method: "patch",
      data: {item_id, quantity, price},
    });
    alert ('Add to cart successfully');
    $(window.location.replace("/"));
  }else{
    alert ('You must sign in before adding new items!');
    $(window.location.replace("/users/sign_in"));
  };   
  });
})