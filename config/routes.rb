Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  
  root 'categories#index'

  post 'items/:id/add_to_cart', to: "items#add_to_cart"
  # resources :carts, only: [:index, :create, :edit, :update, :destroy]
  resources :categories, only: [:index, :show] do
    collection do
      post :search
      get :autocomplete
    end
  end
  resources :items, only: [:index, :show, :update]
  resources :comments, only: [:create, :index, :show, :new]

  # post 'categories/search', to: 'categories#search'
  get 'carts/index'
  post 'carts/create'
  patch 'carts/update'
  get 'carts/destroy'
  
  resources :orders, only: [:index, :show, :create, :new, :destroy]
  resources :ratings, only: [:update]
  namespace :admin do
  	root 'categories#index'

  	resources :categories do
    	resources :items
      collection do
        post :search
        get :autocomplete
      end
    end
  end
end
